# Build VM From Scratch
```mermaid
sequenceDiagram
    AWX Server ->> Netbox: Python script pulls inventory from Netbox into AWX
    AWX Server ->> Gitlab Server: Pull the latest version of a project based on the AWX template
    AWX Server ->> TFTP Server: Creates reservations and restarts dhcpd service
    AWX Server ->> Repo Server: Pulls the initrd.img and vmlinuz from the current repo
    AWX Server ->> AWX Server: Generate pxe and kickstart files
    AWX Server ->> TFTP Server: Copy over initrd.img, vmlinuz, pxe and kickstart files to TFTP Server
    AWX Server ->> Physical Machine: If its a physical machine, WOL
    AWX Server ->> VCSA Server: If its a VM, create VM and power it on
    VM/Physical Machine ->> TFTP Server: DHCP Boot, pull PXE and kickstart files
    VM/Physical Machine ->> Repo Server: Pull and install packages
    loop Built yet?
        AWX Server->>AWX Server: Keep checking if the host is up by trying to connect to ssh
    end
    AWX Server ->> TFTP Server: Clean up pxe and kickstart files on TFTP Server
    AWX Server ->> Gitlab Server: Pull the global_tasks config context and run on target
    AWX Server ->> Gitlab Server: Pull the role specific config context and run on target
```
[[_TOC_]]

## Configure Netbox
When creating a VM inside netbox, ensure you fill out the following at a minimum:
- [ ] Name (Will be the name of the VM in vSphere)
- [ ] Role (Applies the specific config context)
- [ ] Cluster (The name of the vSphere cluster to set the VM to when it is created)
- [ ] vCPUs
- [ ] Memory (Make sure you do this in MB, so 4 GB of memory would be 4096)
- [ ] Disk

If you so choose to specify a CentOS version other than the default for the Role the VM will be playing, you can set that in the Local Config Context Data area. Otherwise the config context that will be applied to your role should look similar to:
```
{
    "centos_ver": "7",
    "datacenter": "Datacenter",
    "pip_url": "http://10.0.75.113/pip/simple",
    "tftp_host": "10.0.75.1",
    "tftp_host_ssh_port": "12345",
    "vm_datastore": "VMFS",
    "vm_folder": "VM_Folder",
    "workstation_ssh_port": "12345"
}
```
You will also have to specify the repo Config Context which should look similar to:
```
{
    "centos_repos": {
        "7_Base": {
            "baseurl": "http://10.0.75.114/yum/centos/7/os/x86_64",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.114/yum/centos/7/os/x86_64/RPM-GPG-KEY-CentOS-7"
        },
        "7_EPEL": {
            "baseurl": "http://10.0.75.114/yum/misc/epel/7/x86_64",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.114/yum/misc/epel/RPM-GPG-KEY-EPEL-7"
        },
        "7_Extras": {
            "baseurl": "http://10.0.75.114/yum/centos/7/extras/x86_64",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.114/yum/centos/7/os/x86_64/RPM-GPG-KEY-CentOS-7"
        },
        "7_Updates": {
            "baseurl": "http://10.0.75.114/yum/centos/7/updates/x86_64",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.114/yum/centos/7/os/x86_64/RPM-GPG-KEY-CentOS-7"
        },
        "8_AppStream": {
            "baseurl": "http://10.0.75.113/yum/centos/8/AppStream/",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.113/yum/centos/8/RPM-GPG-KEY-CentOS-Official"
        },
        "8_BaseOS": {
            "baseurl": "http://10.0.75.113/yum/centos/8/BaseOS/",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.113/yum/centos/8/RPM-GPG-KEY-CentOS-Official"
        },
        "8_elrepo": {
            "baseurl": "http://10.0.75.113/yum/centos/8/elrepo/",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.113/yum/centos/8/elrepo/RPM-GPG-KEY-elrepo.org"
        },
        "8_epel": {
            "baseurl": "http://10.0.75.113/yum/centos/8/epel/",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.113/yum/centos/8/epel/RPM-GPG-KEY-EPEL-8"
        },
        "8_extras": {
            "baseurl": "http://10.0.75.113/yum/centos/8/extras/",
            "gpg_check": "yes",
            "gpg_key": "http://10.0.75.113/yum/centos/8/RPM-GPG-KEY-CentOS-Official"
        }
    }
}
```
And last but not least is the repos and tasks playbooks you want to run after the machine is deployed:
```
{
    "Gitlab-Server": {
        "1": {
            "awx_playbook": "install_gitlab_ce.yml",
            "gitlab_group": "ansible-projects",
            "gitlab_project": "install_gitlab_ce.git"
        },
        "2": {
            "awx_playbook": "broadcast_complete.yml",
            "gitlab_group": "ansible-projects",
            "gitlab_project": "broadcast_complete.git"
        }
    },
    "global_tasks": {
        "1": {
            "awx_playbook": "global_provision.yml",
            "gitlab_group": "ansible-projects",
            "gitlab_project": "global_provisioning.git"
        }
    },
    "gitlab_baseurl": "gitlab.test.lab"
}
```
Once you are done creating the VM, ensure you create an interface and associate an IP for it. Here are the minimums:
- [ ] Name (This can be anything you want. Kickstart will set kernel variables so that its named that way)
- [ ] MAC Address (This can be anything you want, but it must be unique)
- [ ] 802.1Q Mode (This should be set to Access)
- [ ] Untagged vlan (Set this to the VLAN for PXE)

And now make sure you add an IP to the above interface and make sure it is set as the primary IP for the VM. IF YOU DON'T, AWX WILL ERROR OUT TRYING TO IMPORT DATA!!!

If you want to WOL a physical machine to install on it, here is your checklist in netbox:
- [ ] Name (Something unique)
- [ ] Device Role (Apply a config context to this role specifying the device_type : hardware so you don't have to do it in each local config contxt. Also make sure that config context has a Weight HIGHER than your standard one so it will over-ride the standard value of VM)
- [ ] Device Type
- [ ] Site

Just Like the VM, you need to crate an interface for your new hardware machine.
NOTE: Make sure you add ALL physical interfaces the device has into netbox. If you don't, you will run into issues with the naming scheme. YOU HAVE BEEN WARNED!

## Disable SELinux on the TFTP server
I know its bad pratice, but you can go through allowing everything through selinux if you want. Probably needs to get done, but disabling for now.
NOTE: This was designed for CentOS 8+ (IE: firewallcmd as opposed to iptables)
```
setenforce Permissive
```
## Install python3 on the TFTP server
If you don't install python3 on the TFTP server you won't be able to do any ansible things on that server. IE: Copy files to it.
```
yum install -y python3
```
## Install and setup DHCP
NOTE: This was based off a 10.0.75.x/24 network. Change as approperiate. The last line MUST have the #DHCP RESERVATIONS. This is used to add reservations AFTER this line.
```
yum install -y dhcp-server

cat << EOF >> /etc/dhcp/dhcpd.conf
option space pxelinux;
option pxelinux.magic code 208 = string;
option pxelinux.configfile code 209 = text;
option pxelinux.pathprefix code 210 = text;
option pxelinux.reboottime code 211 = unsigned integer 32;
option architecture-type code 93 = unsigned integer 16;

option pxelinux.mtftp-ip    code 1 = ip-address;
option pxelinux.mtftp-cport code 2 = unsigned integer 16;
option pxelinux.mtftp-sport code 3 = unsigned integer 16;
option pxelinux.mtftp-tmout code 4 = unsigned integer 8;
option pxelinux.mtftp-delay code 5 = unsigned integer 8;

option domain-name "test.lab";
option domain-name-servers 192.168.200.98, 192.168.200.99;
default-lease-time 600;
max-lease-time 7200;
authoritative;
log-facility local7;
allow booting;
allow bootp;
option client-system-arch code 93 = unsigned integer 16;

subnet 10.0.75.0 netmask 255.255.255.0 {
  range 10.0.75.10 10.0.75.40;
  option broadcast-address 10.0.75.255;
  option routers 10.0.75.240;

  class "pxeclients" {
    match if substring(option vendor-class-identifier, 0, 9) = "PXEClient";
    next-server 10.0.75.1;

    if option client-system-arch = 00:07{
      filename = "shimx64.efi";
    } else {
      filename = "pxelinux.0";
    }
  }
}
#DHCP RESERVATIONS
EOF

systemctl enable dhcpd
systemctl start dhcpd
firewall-cmd --add-service=dhcp --permanent
firewall-cmd --reload
```

## Install and setup HTTPD
We use HTTPD to serve up the kickstart files. We could probably more them somewhere else at another date.
```
yum install -y httpd
systemctl enable httpd
systemctl start httpd
firewall-cmd --zone=public --add-service=http --permanent
firewall-cmd --reload
```

## Install and setup tftp-server
```
yum install -y tftp-server

cp /usr/lib/systemd/system/tftp.service /etc/systemd/system/tftp-server.service
cp /usr/lib/systemd/system/tftp.socket /etc/systemd/system/tftp-server.socket

cat << EOF >> /etc/systemd/system/tftp-server.service
[Unit]
Description=Tftp Server
Requires=tftp-server.socket
Documentation=man:in.tftpd

[Service]
ExecStart=/usr/sbin/in.tftpd -c -p -s /var/lib/tftpboot
StandardInput=socket

[Install]
WantedBy=multi-user.target
Also=tftp-server.socket
EOF

systemctl start tftp-server
firewall-cmd --zone=public --add-service=tftp --permanent
firewall-cmd --reload
```
## Prep files needed to PXE CentOS
You must have a CentOS ISO attached to the VM in order for this to work. Also, the initrd.img and vmlinuz will automatically be copied where they belong by the ansible playbook when a VM is deployed. This helps keep the repo and TFTP server in sync.
```
mkdir -p /var/lib/tftpboot/{pxelinux.cfg,images}
mkdir /mnt/cd
mount -o loop /dev/cdrom /mnt/cd/
cd
mkdir tmp
cp -pr /mnt/cd/BaseOS/Packages/syslinux-tftpboot-<VERSION>.el8.<ARCH>.rpm tmp/
cd tmp
rpm2cpio syslinux-tftpboot-<VERSION>.el8.<ARCH>.rpm | cpio -dimv
cp tftpboot/{ldlinux.c32,libcom32.c32,libutil.c32,pxelinux.0,vesamenu.c32} /var/lib/tftpboot/
cd ..
rm -rf tmp/
```
For a UEFI install, do the following as well:
```
mkdir tmp
cp -pr /mnt/cd/BaseOS/Packages/grub2-efi-<ARCH>-<VERSION>.el8.<ARC>.rpm tmp/
cp -pr /mnt/cd/BaseOS/Packages/shim-<ARCH>-<VERSION>.el8.<ARCH>.rpm tmp/
cd tmp/
rpm2cpio grub2-efi-<ARCH>-<VERSION>.el8.<ARC>.rpm | cpio -dimv
rpm2cpio shim-<ARCH>-<VERSION>.el8.<ARCH>.rpm | cpio -dimv
cp boot/efi/EFI/centos/shimx64.efi /var/lib/tftpboot/
cp boot/efi/EFI/centos/grubx64.efi /var/lib/tftpboot/
cd ..
rm -rf tmp/
cat << EOF >> /var/lib/tftpboot/grub.cfg
set timeout=60
menuentry 'CentOS 8' {
  linuxefi images/centos/8/vmlinuz ip=dhcp inst.repo=http://10.0.75.113/yum/centos/8/BaseOS/
  initrdefi images/centos/8/initrd.img
}
EOF

cat << EOF >> /var/lib/tftpboot/pxelinux.cfg/default
DEFAULT vesamenu.c32
MENU TITLE Booting local disk (ESC to stop)
timeout 300

display boot.msg

LABEL local
    MENU LABEL Default local boot
    MENU DEFAULT
    LOCALBOOT 0
label centos8
    menu label Install CentOS 8
    kernel images/centos/8/vmlinuz
    append initrd=images/centos/8/initrd.img ip=dhcp inst.repo=http://10.0.75.113/yum/centos/8/BaseOS
label centos7
    menu label Install CentOS 7
    kernel images/centos/7/vmlinuz
    append initrd=images/centos/7/initrd.img ip=dhcp inst.repo=http://10.0.75.114/yum/centos/7/os/x86_64
EOF

chmod 755 /var/lib/tftpboot/
```
